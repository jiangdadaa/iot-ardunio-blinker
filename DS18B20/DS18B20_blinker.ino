#define BLINKER_WIFI //定义wifi模块

#include <Blinker.h>//包含Blinker头文件

#include<OneWire.h>
#include<DallasTemperature.h>

#define PIN 13      //定义DHT11模块连接管脚io2
 

int count=0;
bool WIFI_Status = true;

char auth[] = "fc24be1bb07b";  //你的设备key

OneWire onewire(PIN);
DallasTemperature sensors(&onewire);

 //新建数据类型组件对象，作用：将数据传输到手机blinker app

BlinkerNumber TEMP("temp");    //定义温度数据键名
float temp_read = 0;//定义浮点型全局变量 储存传感器读取的温湿度数据
 
void heartbeat()
{
    TEMP.print(temp_read);        //给blinkerapp回传温度数据
}
void smartConfig()//配网函数
{
  WiFi.mode(WIFI_STA);//使用wifi的STA模式
  Serial.println("\r\nWait for Smartconfig...");//串口打印
  WiFi.beginSmartConfig();//等待手机端发出的名称与密码
  //死循环，等待获取到wifi名称和密码
  while (1)
  {
    //等待过程中一秒打印一个.
    Serial.print(".");
    delay(1000);                                             
    if (WiFi.smartConfigDone())//获取到之后退出等待
    {
      Serial.println("SmartConfig Success");
      //打印获取到的wifi名称和密码
      Serial.printf("SSID:%s\r\n", WiFi.SSID().c_str());
      Serial.printf("PSW:%s\r\n", WiFi.psk().c_str());
      break;
    }
  }
}
void WIFI_Init()
{
    Serial.println("\r\n正在连接");
    //当设备没有联网的情况下，执行下面的操作
    while(WiFi.status()!=WL_CONNECTED)
    {
        if(WIFI_Status)//WIFI_Status为真,尝试使用flash里面的信息去 连接路由器
        {
            Serial.print(".");
            delay(1000);                                        
            count++;
            if(count>=5)
            {
                WIFI_Status = false;
                Serial.println("WiFi连接失败，请用手机进行配网"); 
            }
        }
        else//使用flash中的信息去连接wifi失败，执行
        {
            smartConfig();  //smartConfig技术配网
        }
     }  
     //串口打印连接成功的IP地址
     Serial.println("连接成功");  
     Serial.print("IP:");
     Serial.println(WiFi.localIP());
}

void setup()//将设置代码放在此处，运行一次；
{
    //初始化端口
    Serial.begin(115200);
    BLINKER_DEBUG.stream(Serial);
    BLINKER_DEBUG.debugAll();
    
    WIFI_Init();//调用WIFI函数
    Blinker.begin(auth, WiFi.SSID().c_str(), WiFi.psk().c_str());//运行blinker
    Blinker.attachHeartbeat(heartbeat);//将传感器获取的数据传给blinker app上
}

//通过循环不断读取温湿度传感器获取的数据
void loop() //把主代码放在这里，重复运行：
{
 Blinker.run();//运行Blinker
   sensors.requestTemperatures();
   Serial.write("温度：");
   float t=sensors.getTempCByIndex(0);
   temp_read=t;
   Serial.print(t);
   Serial.println("摄氏度");
   delay(1000);
}